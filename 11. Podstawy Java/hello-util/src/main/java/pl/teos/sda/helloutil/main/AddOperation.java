package pl.teos.sda.helloutil.main;

public class AddOperation {

	private static String operType;
	private String operTypeNonStatic;

	public static int sum(int a, int b) {
		return a + b ;
	}

	public String getOperType(){
		return operType;
	}

	public void setOperType(String newOperType){
		this.operType = newOperType;
	}

}
