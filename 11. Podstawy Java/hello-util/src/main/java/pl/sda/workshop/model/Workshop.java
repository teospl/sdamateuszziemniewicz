package pl.sda.workshop.model;

import java.util.List;

public class Workshop {

	private static int counter;

	private String name;
	private Address address;
	private Owner owner;
	private List<Employee> employees;

	public Workshop(String name) {
		this.name = name;
		counter++;
	}

	public String getName() {
		return name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public static int getCounter() {
		return counter;
	}

	public void setName(String name) {
		this.name = name;
	}


}
