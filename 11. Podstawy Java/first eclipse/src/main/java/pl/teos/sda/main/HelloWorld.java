package pl.teos.sda.main;

import java.util.ArrayList;
import java.util.List;

import pl.sda.workshop.model.Address;
import pl.sda.workshop.model.Employee;
import pl.sda.workshop.model.Owner;
import pl.sda.workshop.model.Workshop;
import pl.teos.sda.helloutil.main.AddOperation;
//import pl.teos.sda.helloutil.main.AddOperation;


public class HelloWorld {

	public static void main(String[] args) {
		//System.out.println("Hello world");
		//System.out.println(AddOperation.sum(1, 4));
		//Workshop w = new Workshop("Pierwejszywarsztaten");
		//w.setAddress("rumunska 13");
		//System.out.println(w.getName());

		Workshop pierwszy = addWorkshop("Pierwszy warsztat");

		Owner wlascicielPierwszegoWarsztatu = new Owner();
		wlascicielPierwszegoWarsztatu.setName("Imie");
		wlascicielPierwszegoWarsztatu.setLastName("Nazwisko");
		pierwszy.setOwner(wlascicielPierwszegoWarsztatu);

		Address address = new Address();
		address.setCity("Miasto");
		address.setStreet("Uliczna");
		pierwszy.setAddress(address);

		List<Employee> employees = new ArrayList<Employee>();
		employees.add(null);

		for(Employee co_chcemy : employees){
			co_chcemy.setName("pierwszy");
		}

		for(int i = 0; i < employees.size(); i++){
			System.out.println(employees.get(i).getName());
		}

		pierwszy.setEmployees(employees);


		System.out.println("Nazwa: " + pierwszy.getName());
		System.out.println("Wasciciel: " + pierwszy.getOwner().getName() + " " + pierwszy.getOwner().getLastName());
		System.out.println("Adres: " + pierwszy.getAddress().getCity() + " " + pierwszy.getAddress().getStreet() + " " + pierwszy.getAddress().getStreetNo());
	}

	private static Workshop addWorkshop(String name){
		return new Workshop(name);
	}




}
