/**
 * Created by RENT on 2016-08-02.
 */
public class DivideOperation implements Operation {

    private double a;
    private double b;

    public DivideOperation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        if(b == 0){
            throw new DivideByZeroException("Nie można dzielić przez zero");
        }
        return a / b;
    }
}
