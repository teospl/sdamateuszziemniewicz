/**
 * Created by RENT on 2016-08-02.
 */
public class DivideByZeroException extends RuntimeException {

    public DivideByZeroException(String s) {
        super(s);
    }
}
