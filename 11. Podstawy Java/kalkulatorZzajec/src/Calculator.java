/**
 * Created by teos on 2016-08-02.
 */
public class Calculator {
    public static void main(String[] args) {
        Operation addOperation = new AddOperation(2, 6);
        double result = addOperation.calculate();
        System.out.println("Wynik = " + result);

        try{
            Operation divideOperation = new DivideOperation(2, 6);
            double wynik = divideOperation.calculate();
            System.out.println(wynik);
        } catch (DivideByZeroException e){
            System.out.println(e.getMessage());
        } catch (Exception e){
            System.out.println("Generalne wyjatki");
        }

    }
}
