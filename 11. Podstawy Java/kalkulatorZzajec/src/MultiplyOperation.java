/**
 * Created by RENT on 2016-08-02.
 */
public class MultiplyOperation implements Operation {

    private double a;
    private double b;

    public MultiplyOperation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        return a * b;
    }
}
