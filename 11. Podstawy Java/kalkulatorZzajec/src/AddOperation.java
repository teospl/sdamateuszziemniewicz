/**
 * Created by teos on 2016-08-02.
 */
public class AddOperation implements Operation {

    private double a;
    private double b;

    public AddOperation(double a, double b){
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        return a + b;
    }
}
