/**
 * Created by teos on 2016-08-02.
 */
public interface Operation {
    double calculate ();
}
