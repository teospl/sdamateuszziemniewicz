/**
 * Created by RENT on 2016-08-02.
 */
public class FactorialOperation implements Operation {

    private double a;

    public FactorialOperation(double a){
        this.a = a;
    }

    @Override
    public double calculate() {
        double result = 1;
        for(int i = 1; i <= a; ++i){
            result = result * i;
        }
        return result;
    }
}
