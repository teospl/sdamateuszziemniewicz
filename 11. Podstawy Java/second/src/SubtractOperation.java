/**
 * Created by RENT on 2016-08-01.
 */
public class SubtractOperation implements Operation {
    private double a;
    private double b;

    public SubtractOperation(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculate() {
        return a - b;
    }
}
