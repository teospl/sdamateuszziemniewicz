import java.util.Scanner;

/**
 * Created by RENT on 2016-08-01.
 */
public class Calculator {
    public static void main(String[] args) {
        String wyrazenie;
        Boolean exit = false;
        Scanner scanner = new Scanner(System.in);

        while (exit == false) {
            System.out.println("Podaj wyrazenie");
            wyrazenie = (scanner.nextLine());
            if ("exit".equals(wyrazenie)) {
                exit = true;
            } else {
                Operation odp = OperationFactory.createOperation(wyrazenie);
                System.out.println(odp.calculate());
            }

        }
    }
}
