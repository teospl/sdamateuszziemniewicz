/**
 * Created by RENT on 2016-08-01.
 */
public interface Operation {
    double calculate ();
}
