/**
 * Created by RENT on 2016-08-01.
 */
public class OperationFactory {
    static Operation createOperation (String param){

        // char[] wyrazenieChar = param.toCharArray();
        Double a = 0.0;
        Double b = 0.0;
        String[] wyrazenie = param.split(" ");




        if(wyrazenie.length < 3){
            a = Double.valueOf(wyrazenie[0]);
        } else if ( wyrazenie.length == 3){
            a = Double.valueOf(wyrazenie[0]);
            b = Double.valueOf(wyrazenie[2]);
        }

        //double a = (double) (wyrazenie[0] - '0');
        //double b = (double) (wyrazenie[2] - '0');

        //Double aDouble = Double.valueOf("1");

        Operation wynik = null;

        if("+".equals(wyrazenie[1])){
            wynik = new AddOperation(a, b);
        } else if ("-".equals(wyrazenie[1])){
            wynik = new SubtractOperation(a, b);
        } else if ("*".equals(wyrazenie[1])){
            wynik = new MultiplyOperation(a, b);
        } else if ("/".equals(wyrazenie[1])){
            wynik = new DivideOperation(a, b);
        } else if ("!".equals(wyrazenie[1])){
            wynik = new StrongOperation(a);
        } else if ("ONP".equals(wyrazenie[1])) {

            wynik = new OnpOperation(wyrazenie);
        } else {
            System.out.println("wywalilem sie na operation factory");
        }

        return wynik;
    }

}
