/**
 * Created by RENT on 2016-08-01.
 */
public class StrongOperation implements Operation {

    private double a;

    public StrongOperation(double a) {
        this.a = a;
    }

    @Override
    public double calculate() {

        double wynik = 1;

        for(int i = 1; i <= a; ++i){
            wynik = wynik * i;
        }

        return wynik;
    }


}
