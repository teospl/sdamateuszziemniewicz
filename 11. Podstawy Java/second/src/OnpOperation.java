import com.sun.org.apache.xalan.internal.xsltc.compiler.util.StringStack;

/**
 * Created by RENT on 2016-08-01.
 */
public class OnpOperation implements Operation {
    private Double a;
    private Double b;
    private String[] inputArray;
   // private String[] tmpStack;

    StringStack tmpStack = new StringStack();

    public OnpOperation(String[] wyrazenie) {
        this.inputArray = wyrazenie;
    }

    @Override
    public double calculate() {

        for(int i = 0; i < inputArray.length; ++i){
            if(!(inputArray[i].isNaN()){
                tmpStack.push(inputArray[i]);
                System.out.println("Wykryto liczbę: " + inputArray[i] + " | Stos: " + tmpStack);
            } else {
                a = Double.valueOf(tmpStack.pop());
                b = Double.valueOf(tmpStack.pop());

                switch (inputArray[i]){
                    case "+":
                        tmpStack.push(b + a);
                        break;
                    case "-":
                        tmpStack.push(b - a);
                        break;
                    case "*":
                        tmpStack.push(b * a);
                        break;
                    case "/":
                        tmpStack.push(b / a);
                        break;
                    default:
                        break;
                }
                System.out.println("Wykryto znak: " + inputArray[i] + " | Stos: " + tmpStack);
            }
        }
        return tmpStack;
    }
}
