package pl.mziem.java.helloWorld;

/**
 * Created by RENT on 2016-07-30.
 */
public class FromWide implements Control{

    @Override
    public void draw(int starsNumber) {
        for (int i = starsNumber; i > 0 ; --i) {
            for (int j = i; j >0 ; --j) {
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}
