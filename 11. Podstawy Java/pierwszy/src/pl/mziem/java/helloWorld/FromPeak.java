package pl.mziem.java.helloWorld;

/**
 * Created by RENT on 2016-07-30.
 */
public class FromPeak implements Control {

    @Override
    public void draw(int starsNumber) {
        for (int i = 0; i <= starsNumber ; ++i) {
            for (int j = 0; j < i ; ++j) {
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}
