package pl.mziem.java.helloWorld;

import java.util.Scanner;

/**
 * Created by RENT on 2016-07-30.
 */
public class HelloWorld {

    public static void main(String[] args) {

        int starsNumber;
        int drawingMethod;
        Scanner scanner = new Scanner(System.in);


        System.out.println("Podaj liczbe gwiazdek");
        starsNumber = (scanner.nextInt());
        System.out.println("Podaj opcję 1/2");
        drawingMethod = (scanner.nextInt());


        drawing(starsNumber, drawingMethod);
    }



    public static void drawing(int starsNumber, int drawingMethod){
        Control fromPeak = new FromPeak();
        Control fromWide = new FromWide();

        if(drawingMethod == 1){
            fromPeak.draw(starsNumber);
        } else if(drawingMethod == 2) {
            fromWide.draw(starsNumber);
        } else {
            System.out.println("Error");
        }

    }




}
