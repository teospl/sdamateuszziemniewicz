import java.util.Random;

/**
 * Created by RENT on 2016-08-02.
 */
public class ExceptionExample {
    public static void main(String[] args) {
//        String[] arrayOfStrings = new String[]{"Ala", "ma", "kota"};
//        String[] nullArray = null

//        try {
//            printElement(arrayOfStrings, 2);
//            printElement(null, 1);
//        } catch (ArrayIndexOutOfBoundsException e){
//            System.out.println("Index z obiektu poza zakresem");
//        } catch (Exception e){
//            System.out.println("Null pointer zioom " + e.toString());
//        } finally {
//            System.out.println("siemson");
//        }

        errorRetry();
    }

    public static void printElement(String[] arrayOfStrings, int index) throws Exception {
        if (index < 0) {
            throw new Exception("Index mniejszy od zera");
        }
        System.out.println(arrayOfStrings[index]);
    }

    public static void errorRetry(){
        String[] arrayOfStrings = new String[]{"Ala", "ma", "kota"};
        String[] nullArray = null;

        boolean retry = false;
        int count = 0;
        do{
            try {
                String[] in = new Random().nextBoolean() ? arrayOfStrings : nullArray;
                printElement(in, 1);
                retry = false;
            } catch (Exception e){
                System.out.println("wyjatek " + e.getMessage());
                retry = true;
                count++;
                }
        } while (retry && count < 4);
            System.out.println("Done");
    }

}
